;;;; -*- Mode: Lisp -*-

;;;; seq-funs-package.lisp --
;;;; Some functions taken from:
;;;; http://haskell.org/ghc/docs/latest/html/libraries/base-4.0.0.0/Data-List.html#10

;;;; See COPYING file for copyright and licensing information.

(defpackage  "IT.UNIMIB.DISCO.MA.CL.EXT.SEQUENCES.LAZY" (:use "COMMON-LISP" "CLAZY")
  (:nicknames
   "CL.EXTENSIONS.SEQUENCES.LAZY"
   "CL.EXT.SEQUENCES.LAZY"
   "LAZY-SEQUENCES"
   "LAZY-SEQS"
   "LAZY-SEQ-UTILS")

  (:export
   "SEQ-INDEX"
   "LIST-INDEX"

   "TAKE"
   "DROP"
   "SPLIT-AT"
   "TAKE-WHILE"
   "DROP-WHILE"
   "SPAN"
   "SEPARATE"

   "STRIP-PREFIX"
   "INITS"
   "TAILS"
   "GROUP-BY"
   "GROUP"

   "PREFIXP"
   "SUFFIXP"
   "INFIXP"
   )

  (:export "DISTINCT")
  )

;;;; end of file -- seq-funs-package.lisp --
