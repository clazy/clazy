;;;; -*- Mode: Lisp -*-

;;;; lambda-list-parsing.lisp --
;;;; Lazy evaluation for Common Lisp.
;;;;
;;;; Lambda List parsing.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "LAZY")


(defun parse-ordinary-lambda-list (lambda-list)
  "Parses a lambda list and returns each group of arguments as a separate value."
  (let ((vars ())
        (optionals ())
        (keywords ())
        (rest ())
        (auxiliary ())
        )
    (labels ((parse-named-arguments (lambda-list &aux (head (first lambda-list)))
               (cond ((null lambda-list) nil)
                     ((consp head)
                      (push head vars)
                      (parse-named-arguments (rest lambda-list)))
                     ((and (symbolp head)
                           (member head lambda-list-keywords))
                      (case head
                        (&optional (parse-optional-arguments (rest lambda-list)))
                        (&key (parse-keyword-arguments (rest lambda-list)))
                        (&aux (parse-auxiliary-arguments (rest lambda-list)))
                        (&rest (parse-rest-arguments (rest lambda-list)))
                        (otherwise
                         (warn "Keyword ~A is implementation dependent.~@
                                The parsing may not work properly."
                               head)
                         (skip-until-next-lambda-list-keyword (rest lambda-list))
                         ))
                      )
                     ((symbolp head)
                      (push head vars)
                      (parse-named-arguments (rest lambda-list)))
                     ))

             (parse-optional-arguments (lambda-list &aux (head (first lambda-list)))
               (cond ((null lambda-list) nil)
                     ((consp head)
                      (push head optionals)
                      (parse-optional-arguments (rest lambda-list)))
                     ((and (symbolp head)
                           (member head lambda-list-keywords))
                      (case head
                        (&optional (error 'program-error))
                        (&key (parse-keyword-arguments (rest lambda-list)))
                        (&aux (parse-auxiliary-arguments (rest lambda-list)))
                        (&rest (parse-rest-arguments (rest lambda-list)))
                        (otherwise
                         (warn "Keyword ~A is implementation dependent.~@
                                The parsing may not work properly."
                               head)
                         (skip-until-next-lambda-list-keyword (rest lambda-list))
                         ))
                      )
                     ((symbolp head)
                      (push head optionals)
                      (parse-optional-arguments (rest lambda-list)))
                     ))

             (parse-keyword-arguments (lambda-list &aux (head (first lambda-list)))
               (cond ((null lambda-list) nil)
                     ((consp head)
                      (push head keywords)
                      (parse-keyword-arguments (rest lambda-list)))
                     ((and (symbolp head)
                           (member head lambda-list-keywords))
                      (case head
                        (&optional (error 'program-error))
                        (&key (error 'program-error))
                        (&aux (parse-auxiliary-arguments (rest lambda-list)))
                        (&rest (parse-rest-arguments (rest lambda-list)))
                        (&allow-other-keys
                         (unless (or (null (rest lambda-list))
                                     (eql (cadr lambda-list) '&aux))
                           (error 'program-error))
                         (skip-until-next-lambda-list-keyword (rest lambda-list)))
                        (otherwise
                         (warn "Keyword ~A is implementation dependent.~@
                                The parsing may not work properly."
                               head)
                         (skip-until-next-lambda-list-keyword (rest lambda-list))
                         ))
                      )
                     ((symbolp head)
                      (push head keywords)
                      (parse-keyword-arguments (rest lambda-list)))
                     ))

             (parse-rest-arguments (lambda-list &aux (head (first lambda-list)))
               (cond ((null lambda-list) nil)
                     ((consp head)
                      (push head rest)
                      ;; Error checking here.
                      (parse-rest-arguments (rest lambda-list)))
                     ((and (symbolp head)
                           (member head lambda-list-keywords))
                      (case head
                        (&optional (error 'program-error))
                        (&key (parse-keyword-arguments (rest lambda-list)))
                        (&aux (parse-auxiliary-arguments (rest lambda-list)))
                        (&rest (error 'program-error))
                        (otherwise
                         (warn "Keyword ~A is implementation dependent.~@
                                The parsing may not work properly."
                               head)
                         (skip-until-next-lambda-list-keyword (rest lambda-list))
                         ))
                      )
                     ((symbolp head)
                      (push head rest)
                      (parse-rest-arguments (rest lambda-list)))
                     ))

             (parse-auxiliary-arguments (lambda-list &aux (head (first lambda-list)))
               (cond ((null lambda-list) nil)
                     ((consp head)
                      (push head auxiliary)
                      (parse-auxiliary-arguments (rest lambda-list)))
                     ((and (symbolp head)
                           (member head lambda-list-keywords))
                      (case head
                        (&optional (error 'program-error))
                        (&key (error 'program-error))
                        (&aux (error 'program-error))
                        (&rest (error 'program-error))
                        (otherwise
                         (warn "Keyword ~A is implementation dependent.~@
                                The parsing may not work properly."
                               head)
                         (skip-until-next-lambda-list-keyword (rest lambda-list))
                         ))
                      )
                     ((symbolp head)
                      (push head auxiliary)
                      (parse-auxiliary-arguments (rest lambda-list)))
                     ))

             (skip-until-next-lambda-list-keyword (lambda-list &aux (head (first lambda-list)))
               (cond ((null lambda-list) nil)
                     ((consp head)
                      (skip-until-next-lambda-list-keyword (rest lambda-list)))
                     ((and (symbolp head)
                           (member head lambda-list-keywords))
                      (case head
                        (&optional (parse-optional-arguments (rest lambda-list)))
                        (&key (parse-keyword-arguments (rest lambda-list)))
                        (&aux (parse-auxiliary-arguments (rest lambda-list)))
                        (&rest (parse-rest-arguments (rest lambda-list)))
                        (otherwise
                         (warn "Keyword ~A is implementation dependent.~@
                                The parsing may not work properly."
                               head)
                         (skip-until-next-lambda-list-keyword (rest lambda-list))
                         ))
                      )
                     ((symbolp head)
                      (skip-until-next-lambda-list-keyword (rest lambda-list)))
                     ))
             )
      (parse-named-arguments lambda-list)
      (values (nreverse vars)
              (nreverse optionals)
              (nreverse keywords)
              (nreverse rest)
              (nreverse auxiliary))
      )))

;;;; end of file -- lambda-list-parsing.lisp --
