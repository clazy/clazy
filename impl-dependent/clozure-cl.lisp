;;;; -*- Mode: Lisp -*-

;;;; clozure-cl.lisp --
;;;; CLAZY implementation dependency file.

(in-package "CLAZY")

(defun arglist (function-designator)
  (ccl:arglist function-designator))

;;;; end of file -- clozure-cl.lisp --
