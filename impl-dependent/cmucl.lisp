;;;; -*- Mode: Lisp -*-

;;;; cmucl.lisp --
;;;; CLAZY implementation dependency file.

(in-package "CLAZY")

(defun arglist (function-designator)
  (ext:arglist (or (macro-function function-designator)
		   (symbol-function function-designator))))

;;;; end of file -- cmucl.lisp --
