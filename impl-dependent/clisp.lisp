;;;; -*- Mode: Lisp -*-

;;;; clisp.lisp --
;;;; CLAZY implementation dependency file.

;;;; See COPYING file for copyright and licensing information.

(in-package "CLAZY")

(defun arglist (function-designator)
  (ext:arglist (or (macro-function function-designator)
		   (symbol-function function-designator))))

;;;; end of file -- clisp.lisp --
