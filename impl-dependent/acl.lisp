;;;; -*- Mode: Lisp -*-

;;;; acl.lisp --
;;;; CLAZY implementation dependency file.

(in-package "CLAZY")

(defun arglist (function-designator)
  (excl:arglist function-designator))

;;;; end of file -- lispworks.lisp --
