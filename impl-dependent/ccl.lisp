;;;; -*- Mode: Lisp -*-

;;;; ccl.lisp --
;;;; CLAZY implementation dependency file.

(in-package "CLAZY")

(defun arglist (function-designator)
  (ccl:arglist (or (macro-function function-designator)
		   (symbol-function function-designator))))

;;;; end of file -- ccl.lisp --
