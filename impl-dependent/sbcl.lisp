;;;; -*- Mode: Lisp -*-

;;;; sbcl.lisp --
;;;; CLAZY implementation dependency file.
;;;;
;;;; See file COPYING for copyright and licensing information.

(in-package "CLAZY")

(eval-when (:load-toplevel :compile-toplevel :execute)
  (require :sb-introspect))

(defun arglist (function-designator)
  (sb-introspect:function-lambda-list function-designator))

;;;; end of file -- sbcl.lisp --
