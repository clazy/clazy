;;;; -*- Mode: Lisp -*-

;;;; clazy-package.lisp --
;;;; Lazy evaluation for Common Lisp.
;;;;
;;;; See file COPYING for copyright and licensing information.

(defpackage "IT.UNIMIB.DISCO.LAZY" (:use "CL")
  (:nicknames "LAZY"
              "CL.EXTENSIONS.DATA-AND-CONTROL-FLOW.LAZY"
              "CL.EXT.DACF.LAZY"
              "CLAZY")

  (:export "UNDEFINED-LAZY-FUNCTION" "INVALID-LAZY-ARGUMENT")
  (:export "DEFLAZY" "DEF-LAZY-FUNCTION" "CALL" "LAZY" "LAZY-FUNCTION")

  (:export "THUNK" "MAKE-THUNK" "THUNK-P" "THUNK-CODE")
  (:export "DELAY" "FORCE")
  (:export "LAZY-LAMBDA"
	   "LAZY-LAMBDA-P"
	   "LAZY-LAMBDA-CODE"
	   "LAZY-LAMBDA-ARGLIST")

  (:export "LET/LAZY")

  (:export "LAZY-STREAM"
           "LAZY-STREAM-P"
           "COPY-LAZY-STREAM"

           "LAZY-CONS"
           "LAZY-CONS-P"
           "COPY-LAZY-STREAM"

           "HEAD"
           "TAIL"
           "CONS-STREAM"

           "REPEATEDLY"
           "SLACKING"
           "LAZILY"
	   "DIVERGE"
           )

  ;; Tests.
  (:export "INTEGERS-FROM" "NATURALS")

  (:documentation "The Common Lisp Extensions Lazy Evaluation Package.")
  )

;;;; end of file -- clazy-package.lisp --
